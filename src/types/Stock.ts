type Stock = {
  id: number
  name: string
  qty: number
  priceUnit: number
}
export { type Stock }
