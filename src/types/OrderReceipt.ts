import type { Employee } from './Employee'
import type { OrderReceiptItem } from './OrderReceiptItem'
import type { User } from './User'

type status = 'delivered' | 'waiting for shipment'

type OrderReceipt = {
  id: number
  orderDate?: Date
  receiveDate?: Date
  totalQty: number
  totalPrice: number
  statusOrder: status[]
  employeeId: number
  employee?: User
  orderReceiptItem?: OrderReceiptItem[]
}
export type { OrderReceipt, status }
