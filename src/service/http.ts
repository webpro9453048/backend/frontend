// frontend/src/services/api.js
import axios from 'axios'

const api = axios.create({
  baseURL: 'http://localhost:3000' // แทนที่ URL ด้วย URL ของ backend ของคุณ
})

export default api
